#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

# Initialise
lamda = 2  # 'lambda' is a protected word
N = 100000
X = np.random.rand(N)
Y = - (1 / lamda) * np.log(X)

# Create Histogram
plt.rc('font', family='serif')

figure = plt.figure(0)
_, bins, _ = plt.hist(Y, bins=100, normed=True, color='#4040ff', lw=0.5)
plt.rcParams["axes.titlesize"] = 10
plt.title('A normalised plot of {} values of the exponentially distributed '
          'random variable Y'.format(N))
linex = np.linspace(0, bins[-1], num=100)
liney = lamda * np.exp(-lamda * linex)
plt.plot(linex, liney, ls='-', marker='', color='black')  # plot line
plt.xlabel('y')
plt.savefig('Y.pdf')
